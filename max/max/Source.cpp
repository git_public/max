#include <iostream>

template<typename T>
T max(T a, T b, T c)
{
	T tmp = (a > b) ? a : b;
	return (tmp > c) ? tmp : c;
}

template<typename T>
void MinMax(T a, T b, T c, T& min, T& max, T(*cmp)(T, T))
{
	T tmp = (cmp(a, b)) ? a : b;
	max = (cmp(tmp, c)) ? tmp : c;
	tmp = (cmp(a, b)) ? b : a;
	min = (cmp(tmp, c)) ? c : tmp;
}

template<typename T>
T Max(T a, T b)
{
	return (a > b) ? a : b;
}

int main(void)
{
	std::cout << max<int>(1, 2, 3) << std::endl;
	std::cout << max<float>(1.3F, 3.4F, 0.5F) << std::endl;
	int min, max;
	MinMax<int>(1, 2, 3, min, max, Max<int>);
	std::cout << min << max << std::endl;
	getc(stdin);
}